# Safety without types: The login function dilemma 

## Problem

We have a system who works with users: creates, updates, deleted them but the most important logs them into a system and out of a system.

So we'd probably have a user like

```
Type User
Properties:
 id: int ( not null , pos)
 username : string ( not null , not empty, at least 8
 password : string ( not null , not empty, at least 8 , one number )
￼age : int ( not null , >= 18 )
```

In Java this will be a class but in Clojure it will be a map ( maybe ) with a spec.

So our functions will be like:

```
1. InsertUser( user: User )
2. UpdateUser ( user:User )
3. DeleteUser( user:User )
4. LoginUser ( user:User ) 
5. LogoutUser ( user:User )
```

But ... wait

* Do we need the id when inserting the user?
* Do we need the need all the properties every time we update? 
* Do we need anything more them the id when deleting?
* Do we need the id and age when logging in?
* Do we need anything more then id for logging out? Or do we need only the username. 

Many questions...

For creating we could pass separate parameters:

*username, password, age*

We'll need to also check each individual value that respect s what we said at the beginning ( not null etc ). Also ... this tends to be bad practice if this user will evolve because each time we add a property we need to change our function. Maybe we create a different type just for this

```
Type usertoinsert
Properties
 ...
```

For updating I want to only update the password. Shall I pass only that or maybe we do the same and create 

```
Type usertoupdate
With maybe properties
```

For deleting we could just use the id.Same for logout.
And for login we could use username and password as strings assuming this will never change. 

But **we ended up with 3 types, 5 functions from which only 2 use the types**. Not happy for such a simple system.

What do dynamic languages do to ensure safety since they don't have types?

## Solution in Clojure

We'll define a spec for a type like

```clojure
(def matcher (re-matcher #"\d+" "abc12345def"))
;data-spec
(s/def ::id nat-int?)
(s/def ::username (s/and
                    string?
                    #(not (empty? %))
                    #(not (< (count %) 8))
                    ))
(s/def ::password (s/and
                    ::username
                    #(not (< (count %) 8))
                    #(not (nil? (re-find #"\d+" %)));contains at least one digit
                    ))
(s/def ::age (s/and nat-int? (fn [x] (< x 150))))
```


Then
```clojure
(s/def ::user (s/keys :opt-un [::id ::username ::password ::age]))
```

Isn't this the same? 

Let's see:
```clojure
;function-spec
(s/fdef insert-user :args (s/cat :user (s/keys :req-un [::username ::password ::age])))
(s/fdef delete-user :args (s/cat :id (s/keys :req-un [::id])))
(s/fdef login-user :args (s/cat :user (s/keys :req-un [::username ::password])))
(s/fdef logout-user :args (s/cat :id (s/keys :req-un [::id])))
(s/fdef update-user :args (s/cat :user (s/keys :req-un [::id] :opt-un [::username ::password ::age])))
``` 

So we pass only what's mandatory each time, always as part of the same spec 
```clojure
(insert-user {})
;Execution error - invalid arguments to tech.danbunea.safety-without-types/insert-user at (safety_without_types.clj:72).
;tech.danbunea.safety-without-types=> {} - failed: (contains? % :username) at: [:user]
;{} - failed: (contains? % :password) at: [:user]
;{} - failed: (contains? % :age) at: [:user]
```

```clojure
(update-user {:username "username" :password "password1" :age 34});fails
;Execution error - invalid arguments to tech.danbunea.safety-without-types/update-user at (safety_without_types.clj:78).
;{:username "username", :password "password1", :age 34} - failed: (contains? % :id) at: [:user]
```

```clojure
(login-user {:username "user" :password "password1"})
;Execution error - invalid arguments to tech.danbunea.safety-without-types/login-user at (safety_without_types.clj:82).
;tech.danbunea.safety-without-types=> "user" - failed: (not (< (count %) 8)) at: [:user :username] spec: :tech.danbunea.safety-without-types/username
```

Going further:
```clojure
  (insert-user {:id 1 :username "username" :password "password1"});fails
  (insert-user {:username "username" :password "password1" :age 34})

  (update-user {:id 1 :username "username" :password "password1"});fails
  (update-user {:id 1 :username "us" :password "password1"});fails
  (update-user {:username "username" :password "password1" :age 34});fails
  (update-user {:id 1})

  (login-user {:id 1});fails
  (login-user {:username "user" :password "password1"});fails
  (login-user {:username "username" :password "password1"});fails

  (logout-user {:id 1})
  (logout-user {:id nil});fails

  (delete-user {:id 1})
  (delete-user {:id nil});fails
```

![Demo](/demo.mov)

## Conclusion

Clojure (but also Elixir) do offer options for safety without types. In Clojure it's clojure.spec, and you can specify data as well as functions and in the end 
even generate data based on specs or check functions using their spec.

For a deeper dive into clojure.spec https://www.youtube.com/watch?v=Xb0UhDeHzBM


## Installation

Download from https://github.com/tech.danbunea/safety-without-types.

## Usage

FIXME: explanation

Run the project directly:

    $ clojure -m tech.danbunea.safety-without-types

Run the project's tests (they'll fail until you edit them):

    $ clojure -A:test:runner

Build an uberjar:

    $ clojure -A:uberjar

Run that uberjar:

    $ java -jar safety-without-types.jar


## License

Copyright © 2020 Danbunea

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
